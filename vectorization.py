from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd


def vectorization(training_data,testing_data):
	train_columns = []
	test_columns = []
	for train_column, test_column in zip(training_data,testing_data):
		vectorizer = TfidfVectorizer(stop_words = 'english',ngram_range=(1,2), lowercase = True,min_df=30, max_df=0.5)
		res1 = vectorizer.fit_transform(train_column.values.astype('U'))
		res2 = vectorizer.transform(test_column.values.astype('U'))
		train = pd.DataFrame(res1.todense(), columns=vectorizer.get_feature_names())
		train_columns.append(train)
		test = pd.DataFrame(res2.todense(), columns=vectorizer.get_feature_names())
		test_columns.append(test)

	x_train	= pd.concat(train_columns,axis=1)
	x_test = pd.concat(test_columns,axis=1)

	return (x_train,x_test)
