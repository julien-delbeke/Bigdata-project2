# Big Data Project by Julien Delbeke, Nestor Eduardo Ramos Perez & Antonio Rafaele

# Know which file are spam or ham in the training set -> determined in file spam-mail.tr.label

# Two sets: TR for the training set & TT for the testing set. 

import email.parser 
from sys import exit
from os import listdir
from os.path import exists
import pandas as pd
import re

CONTENT = "Content"

def extract(directory,features):
	#Extract desired features
	if not exists(directory):
		print("The directory does not exist")
		exit()
	files = listdir(directory)
	all_extracted=[features.copy()]
	for file in files:
		msg = email.message_from_file(open(directory + "\\" + file,errors='ignore'))
		extraction_one_file = [file[file.find("_")+1:file.find(".")]]
		for elem in features : #Ignore ID and prediction element.
			if(elem == CONTENT):
				extraction_one_file.append(get_content(msg))
			else:
				features_res = msg.get_all(elem)
				extraction_one_file.append(sanitise_features(features_res))
		all_extracted.append(extraction_one_file)
	return all_extracted

def sanitise_features(features_res):
	if features_res!=None:
		features_res = ' ;'.join(map(str, features_res))
		features_res = re.sub('\s+',' ',features_res.replace("\t","").replace("\n"," ")) #Remove tab and replace "go to line" by a space. Also remove multiple spaces and tab.
	return features_res

def get_content(msg):
	#gety the contents of an e-mail.
	payload = msg.get_payload()
	if type(payload) == type(list()) :
		payload = payload[0] 									#only use the first part of payload
	if type(payload) != type('') :
		payload = str(payload)
	return re.sub('<[^<]+>',"",payload.replace("\n"," "))		#Remove xml/html expressions


def create_csv(data,dest):
	data[0].insert(0,"ID")
	df = pd.DataFrame.from_records(data)
	df.to_csv(dest, header=None, index=None)

def spam_or_ham():
	f = open("C:\\Users\\Anton\\OneDrive\\Documents\\Unif\\UnifBigData\\spam-mail.tr.label", "r")
	ID_pred = [] # list defined as follow: [ [ID_1,Prediction_1], [ID_2,Prediction_2],...,[ID_2500,Prediction_2500]]
	line = f.readline() #Pass the first line. 
	line = f.readline()
	while line :
		line = line.rstrip("\n")
		split_line = line.split(",")
		ID_pred.append(split_line)
		line = f.readline()
	return ID_pred

def add_spam_ham(TR,res):
	#Add predictions 
	for pos in range(len(TR)-1):
		pos_found = False
		iterator=0
		while not pos_found: 
			if(TR[pos+1][0]==res[iterator][0]):
				TR[pos+1].append(res[iterator][1])
				pos_found=True
			iterator+=1
	TR[0].append("prediction")
	return TR


if __name__ == '__main__':
	
	directory = ["C:\\Users\\Anton\\OneDrive\\Documents\\Unif\\UnifBigData\\TR","C:\\Users\\Anton\\OneDrive\\Documents\\Unif\\UnifBigData\\TT"] #Put the directory where all the ".eml" file are located
	features=["Received","From","To","Subject","Reply-to","In-Reply-To","Received-SPF","DKIM-Signature", "Content"] #Most important features

	#Extract important features
	extractedTR = extract(directory[0],features)
	extractedTR = add_spam_ham(extractedTR,spam_or_ham())
	extractedTT = extract(directory[1],features)

	destination = [r'C:\\Users\\Anton\\OneDrive\\Documents\\Unif\\UnifBigData\\TR_combined.csv',r'C:\\Users\\Anton\\OneDrive\\Documents\\Unif\\UnifBigData\\TT_combined.csv']
	#Create CSV file
	create_csv(extractedTR,destination[0])
	create_csv(extractedTT,destination[1])