import pandas as pd
import vectorization as vect
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


def cleanFrames(training_data,testing_data):
	training_data['From'] = training_data['From'].map(lambda x: x if x.find('<')==-1 else x[x.find('<'):].replace('<','').replace('>',''))
	testing_data['From'] = testing_data['From'].map(lambda x: x if x.find('<')==-1 else x[x.find('<'):].replace('<','').replace('>',''))
	training_data['To'] = training_data['To'].map(lambda x: x if str(x).find('<')==-1 else str(x)[str(x).find('<'):].replace('<','').replace('>',''))
	testing_data['To'] = testing_data['To'].map(lambda x: x if str(x).find('<')==-1 else str(x)[str(x).find('<'):].replace('<','').replace('>',''))

	return training_data,testing_data


training_data = pd.read_csv('TR_combined.csv')
#testing_data = pd.read_csv('TESTING.csv')
testing_data = pd.read_csv('TT_combined.csv')
y_train = training_data['prediction']
#y_test = testing_data['Prediction']

#training_data, testing_data = cleanFrames(training_data,testing_data)
training_data_temp =[training_data['Received'],training_data['From'],training_data['To'],training_data['Subject'],training_data['Reply-to'],training_data['In-Reply-To'],training_data['Received-SPF'],training_data['DKIM-Signature'],training_data['Content']]
testing_data_temp =[testing_data['Received'],testing_data['From'],testing_data['To'],testing_data['Subject'],testing_data['Reply-to'],testing_data['In-Reply-To'],testing_data['Received-SPF'],testing_data['DKIM-Signature'],testing_data['Content']]

x_train, x_test = vect.vectorization(training_data_temp,testing_data_temp)


"""
model = MultinomialNB()
model.fit(x_train, y_train)#.predict(x_test)
predictionGNB= model.predict(x_test)
print("MultinomialNB: "+str(accuracy_score(y_test,predictionGNB)*100))"""


############################################

clf = LinearSVC()
clf.fit(x_train, y_train)
predictionSVC = clf.predict(x_test)

#print("SVC: "+str(accuracy_score(y_test,predictionSVC)*100))


##############################################
"""tree = DecisionTreeClassifier()
tree.fit(x_train, y_train)
predicttionsDecisionTree = tree.predict(x_test)

print("decisionTree: "+str(accuracy_score(y_test,predicttionsDecisionTree)*100))"""

##############################################
"""log = LogisticRegression(solver='lbfgs')
log.fit(x_train, y_train)
predictionLog = log.predict(x_test)
"""
#print("LogisticRegression: "+str(accuracy_score(y_test,predictionLog)*100))


##########Prediction output to upload on Kaggle#########


kaggle_prediction = pd.DataFrame(columns=['Id', 'Prediction'])
kaggle_prediction['Id'] = testing_data['ID']
kaggle_prediction['Prediction'] = predictionSVC

kaggle_prediction.to_csv('jutoted_linearSVC_score.csv' ,index=False)


